use std::convert::TryFrom;
use std::fmt;

use crate::error;

include!(concat!(env!("OUT_DIR"), "/instance_type.rs"));

impl Default for InstanceType {
    fn default() -> Self {
        Self::T2_Nano
    }
}

impl fmt::Display for InstanceType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_str().fmt(f)
    }
}

impl TryFrom<&str> for InstanceType {
    type Error = error::Error;

    fn try_from(name: &str) -> Result<Self, Self::Error> {
        Self::from_text(name).ok_or_else(|| Self::Error::invalid_instance_type(name))
    }
}

impl serde::Serialize for InstanceType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_str())
    }
}
