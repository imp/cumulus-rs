build:
    cargo build
clean:
    cargo clean
test:
    cargo test --all-targets --features yaml
update:
    cargo update
clippy:
    cargo clippy --all-targets --features yaml
bench:
    cargo bench
pedantic:
    cargo clippy --all --all-targets --features pedantic
alias fmt := rustfmt
rustfmt:
    cargo fmt --all -- --check
check: rustfmt update test clippy
lint:
    cargo run --example json > template.json
    cfn-lint template.json
    rm -f template.json
