# AWS CloudFormation Template Generator

## Work in Progress

The purpose is to programmatically generate templates for AWS CloudFormation

Here is a quick example of how it should work when operational (i.e. version >= 0.1.0)

```rust
use cumulus::ec2;
use cumulus::Template;

fn main() {
    let proxy1 = ec2::Instance::new("proxy1").instance_type("s2.micro");
    let proxy2 = ec2::Instance::new("proxy2").instance_type("s2.micro");

    let template = Template::new()
        .description("Proxy Farm")
        .resource(proxy1)
        .resource(proxy2)
        .json_pretty();

    println!("{}", template);
}
```
