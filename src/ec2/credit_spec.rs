use serde::Serialize;

use crate::skip_serializing_if::SkipSerializingIf;

#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct CreditSpecification {
    #[serde(rename = "CPUCredits")]
    cpu_credits: CpuCredits,
}

impl CreditSpecification {
    fn new(cpu_credits: CpuCredits) -> Self {
        Self { cpu_credits }
    }

    pub fn standard() -> Self {
        Self::new(CpuCredits::Standard)
    }

    pub fn unlimited() -> Self {
        Self::new(CpuCredits::Unlimited)
    }
}

impl Default for CreditSpecification {
    fn default() -> Self {
        let cpu_credits = CpuCredits::Standard;
        Self { cpu_credits }
    }
}

impl SkipSerializingIf for CreditSpecification {
    fn is_default(&self) -> bool {
        *self == Self::default()
    }
}

#[derive(Clone, Debug, PartialEq, Serialize)]
#[serde(rename_all = "lowercase")]
enum CpuCredits {
    Standard,
    Unlimited,
}
