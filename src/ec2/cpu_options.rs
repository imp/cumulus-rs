use serde::Serialize;

use crate::skip_serializing_if::SkipSerializingIf;

#[derive(Clone, Debug, PartialEq, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct CpuOptions {
    core_count: u64,
    threads_per_core: u64,
}

impl Default for CpuOptions {
    fn default() -> Self {
        Self {
            core_count: 1,
            threads_per_core: 1,
        }
    }
}

impl SkipSerializingIf for CpuOptions {
    fn is_default(&self) -> bool {
        *self == Self::default()
    }
}
