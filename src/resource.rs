use serde::Serialize;

use super::ec2::EIp;
use super::ec2::Instance;
use super::ec2::Vpc;

#[derive(Debug, Serialize)]
#[serde(tag = "Type", content = "Properties")]
pub enum Resource {
    #[serde(rename = "AWS::EC2::Instance")]
    Instance(Instance),
    #[serde(rename = "AWS::EC2::EIP")]
    EIp(EIp),
    #[serde(rename = "AWS::EC2::VPC")]
    Vpc(Vpc),
}

impl From<Instance> for Resource {
    fn from(instance: Instance) -> Self {
        Self::Instance(instance)
    }
}

impl From<EIp> for Resource {
    fn from(eip: EIp) -> Self {
        Self::EIp(eip)
    }
}

impl From<Vpc> for Resource {
    fn from(vpc: Vpc) -> Self {
        Self::Vpc(vpc)
    }
}

pub trait ToResource: Into<Resource> {
    fn name(&self) -> String;
}
