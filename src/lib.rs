#![doc(html_root_url = "https://docs.rs/cumulus/0.0.6")]
#![cfg_attr(feature = "pedantic", warn(clippy::pedantic))]
#![warn(clippy::use_self)]
#![warn(deprecated_in_future)]
#![warn(future_incompatible)]
#![warn(unreachable_pub)]
#![warn(missing_debug_implementations)]
#![warn(rust_2018_compatibility)]
#![warn(rust_2018_idioms)]
#![warn(unused)]
#![deny(warnings)]

pub mod error;

pub mod condition;
pub mod ec2;
pub mod mapping;
pub mod metadata;
pub mod output;
pub mod parameter;
pub mod resource;
pub mod transform;

mod r#ref;
mod skip_serializing_if;
mod template;

pub use r#ref::{Ref, ValueOrRef};
pub use template::Template;
