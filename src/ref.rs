use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct Ref {
    r#ref: String,
}

impl Ref {
    pub(crate) fn new(r#ref: &str) -> Self {
        let r#ref = r#ref.to_string();
        Self { r#ref }
    }
}

#[derive(Clone, Debug, Serialize)]
#[serde(untagged)]
pub enum ValueOrRef {
    Value(String),
    Ref(Ref),
}

impl From<Ref> for ValueOrRef {
    fn from(r#ref: Ref) -> Self {
        Self::Ref(r#ref)
    }
}

impl<T> From<T> for ValueOrRef
where
    T: Into<String>,
{
    fn from(value: T) -> Self {
        let value = value.into();
        Self::Value(value)
    }
}

impl Default for ValueOrRef {
    fn default() -> Self {
        Self::Value(String::new())
    }
}
