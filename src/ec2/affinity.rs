use serde::Serialize;

use crate::skip_serializing_if::SkipSerializingIf;

#[derive(Clone, Debug, PartialEq, Serialize)]
#[serde(rename_all = "lowercase")]
pub(crate) enum Affinity {
    Default,
    Host,
}

impl Default for Affinity {
    fn default() -> Self {
        Self::Default
    }
}

impl SkipSerializingIf for Affinity {
    fn is_default(&self) -> bool {
        *self == Self::default()
    }
}
