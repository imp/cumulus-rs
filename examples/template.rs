use cumulus::ec2::{EIp, Instance};
use cumulus::parameter::Parameter;
use cumulus::Template;

pub fn template() -> Template {
    let env = Parameter::image_id("ImageId")
        .description("AMI image id for proxy instance")
        .default("ami-0ff8a91507f77f868");

    let proxy1 = Instance::new("proxy1")
        .instance_type("t2.micro")
        .image_id(env.r#ref());
    let proxy2 = Instance::new("proxy2")
        .instance_type("t2.micro")
        .image_id("ami-0ff8a91507f77f867");
    let eip1 = EIp::vpc("proxyip1");
    let eip2 = EIp::vpc("proxyip2");

    let mut template = Template::new();

    template
        .description("Proxy Farm")
        .parameter(env)
        .resource(proxy1)
        .resource(eip1)
        .resource(proxy2)
        .resource(eip2);

    template
}
