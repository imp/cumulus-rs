use serde::Serialize;

use crate::skip_serializing_if::SkipSerializingIf;

#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct Vpc {
    cidr_block: String,
    #[serde(skip_serializing_if = "bool::is_false")]
    enable_dns_hostnames: bool,
    #[serde(skip_serializing_if = "bool::is_false")]
    enable_dns_support: bool,
    #[serde(skip_serializing_if = "Tenancy::is_default")]
    instance_tenancy: Tenancy,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    tags: Vec<String>,
}

impl Default for Vpc {
    fn default() -> Self {
        Self {
            cidr_block: String::new(),
            enable_dns_hostnames: false,
            enable_dns_support: false,
            instance_tenancy: Tenancy::Default,
            tags: Vec::new(),
        }
    }
}

impl Vpc {
    pub fn vpc(cidr_block: impl Into<String>) -> Self {
        let cidr_block = cidr_block.into();
        Self {
            cidr_block,
            ..Self::default()
        }
    }

    pub fn dedicated(cidr_block: impl Into<String>) -> Self {
        let cidr_block = cidr_block.into();
        Self {
            cidr_block,
            instance_tenancy: Tenancy::Dedicated,
            ..Self::default()
        }
    }

    pub fn host(cidr_block: impl Into<String>) -> Self {
        let cidr_block = cidr_block.into();
        Self {
            cidr_block,
            instance_tenancy: Tenancy::Host,
            ..Self::default()
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
#[serde(rename_all = "lowercase")]
enum Tenancy {
    Default,
    Dedicated,
    Host,
}

impl SkipSerializingIf for Tenancy {
    fn is_default(&self) -> bool {
        *self == Self::Default
    }
}
