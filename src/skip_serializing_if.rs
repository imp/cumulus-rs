// Having `is_zero` as a standalone function upsets clippy due to passing
// trivial type by reference. So trait it is.

pub(crate) trait SkipSerializingIf {
    fn is_default(&self) -> bool {
        false
    }

    fn is_zero(&self) -> bool {
        false
    }

    fn is_true(&self) -> bool {
        false
    }

    fn is_false(&self) -> bool {
        false
    }
}

impl SkipSerializingIf for u64 {
    fn is_zero(&self) -> bool {
        *self == 0
    }
}

impl SkipSerializingIf for bool {
    fn is_true(&self) -> bool {
        *self
    }

    fn is_false(&self) -> bool {
        !*self
    }
}
