use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Invalid instance type ({0})")]
    InvalidInstanceType(String),
}

impl Error {
    pub(crate) fn invalid_instance_type(r#type: &str) -> Self {
        let r#type = r#type.to_string();
        Self::InvalidInstanceType(r#type)
    }
}
