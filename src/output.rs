use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct Output {
    description: String,
    value: String,
    export: String,
}
