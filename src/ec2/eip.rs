use serde::Serialize;

use crate::resource::ToResource;
use crate::skip_serializing_if::SkipSerializingIf;

#[derive(Debug, Default, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct EIp {
    #[serde(skip)]
    name: String,
    #[serde(skip_serializing_if = "Domain::is_default")]
    domain: Domain,
    #[serde(skip_serializing_if = "String::is_empty")]
    instance_id: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    public_ipv4_pool: String,
}

impl EIp {
    pub fn vpc(name: impl ToString) -> Self {
        let name = name.to_string();
        Self {
            name,
            domain: Domain::Vpc,
            ..Self::default()
        }
    }
}

impl ToResource for EIp {
    fn name(&self) -> String {
        self.name.clone()
    }
}

#[derive(Debug, PartialEq, Serialize)]
#[serde(rename_all = "lowercase")]
enum Domain {
    Standard,
    Vpc,
}

impl Default for Domain {
    fn default() -> Self {
        Self::Standard
    }
}

impl SkipSerializingIf for Domain {
    fn is_default(&self) -> bool {
        *self == Self::Standard
    }
}
