pub use blockdevice::{BlockDeviceMapping, Ebs};
pub use cpu_options::CpuOptions;
pub use credit_spec::CreditSpecification;
pub use eip::EIp;
pub use instance::Instance;
pub use instance_type::InstanceType;
pub use vpc::Vpc;

pub(crate) use affinity::Affinity;

mod affinity;
mod blockdevice;
mod cpu_options;
mod credit_spec;
mod eip;
mod instance;
mod instance_type;
mod vpc;
