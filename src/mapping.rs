use std::collections::HashMap;

use serde::Serialize;

#[derive(Clone, Debug, Default, Serialize)]
pub struct Mapping(HashMap<String, HashMap<String, String>>);

impl Mapping {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn add(
        &mut self,
        key: impl Into<String>,
        map: impl IntoIterator<Item = (String, String)>,
    ) -> &mut Self {
        let key = key.into();
        let value = map.into_iter().collect();
        self.0.insert(key, value);
        self
    }
}
